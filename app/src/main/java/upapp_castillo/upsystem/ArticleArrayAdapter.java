package upapp_castillo.upsystem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joshua on 11/23/17.
 */

public class ArticleArrayAdapter extends ArrayAdapter<Article> {
    private Context context;
    private List<Article> articles;

    public ArticleArrayAdapter(Context context, int resource, ArrayList<Article> objects) {
        super(context, resource, objects);

        this.context = context;
        this.articles = objects;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Article article = articles.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.cardview_fragment, null);

        TextView title = (TextView) view.findViewById(R.id.title);
        TextView content = (TextView) view.findViewById(R.id.content);

        title.setText(article.getTitle());
        content.setText(article.getContent());

        return view;
    }
}
