package upapp_castillo.upsystem;

import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by joshua on 11/18/17.
 */

public class ArticleFragment extends Fragment {

    View myView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        String content = getArguments().getString("content");
        String title = getArguments().getString("title");
        View view = inflater.inflate(R.layout.article_view_layout, container, false);
        TextView titlev = (TextView) view.findViewById(R.id.title);
        titlev.setText(title);
        TextView contentv = (TextView) view.findViewById(R.id.content);
        contentv.setText(content);
        return view;
    }
}
