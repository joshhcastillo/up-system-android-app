package upapp_castillo.upsystem;

//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by joshua on 11/18/17.
 */

public class BreakthroughsFragment extends Fragment {
    ArrayList titles = new ArrayList<String>();
    ArrayList contents = new ArrayList<String>();
    private ArrayList<Article> articles = new ArrayList<>();
    ListView lv = null;
//    FragmentManager fragmentManager = getFragmentManager();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View myView = inflater.inflate(R.layout.news_layout, container, false);
        lv = myView.findViewById(R.id.listview);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
//                String data = (String) contents.get(position);
                Article a = articles.get(position);
//                a.setContent(a.getContent().replaceAll("<(.*?)\\>"," "));
//                a.setContent(a.getContent().replaceAll("<(.*?)\\\n"," "));
//                a.setContent(a.getContent().replaceFirst("<(.*?)\\>"," "));
//                a.setContent(a.getContent().replaceAll("&nbsp;"," "));
//                a.setContent(a.getContent().replaceAll("&amp;"," "));
//                data = data.replaceAll("<(.*?)\\>"," ");//Removes all items in brackets
//                data = data.replaceAll("<(.*?)\\\n"," ");//Must be undeneath
//                data = data.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket
//                data = data.replaceAll("&nbsp;"," ");
//                data = data.replaceAll("&amp;"," ");
//                bundle.putString("title", a.getTitle());
//                bundle.putString("content", a.getContent());
                bundle.putString("content", a.getContent());
                bundle.putString("title", a.getTitle());
                ArticleFragment articleFragment = new ArticleFragment();
                articleFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, articleFragment).addToBackStack(null).commit();
            }
        });

        new ProcessInBackground().execute();

        return myView;
    }

    public InputStream getInputStream(URL url) {
        try {
            return url.openConnection().getInputStream();
        }
        catch (IOException e) {
            return null;
        }
    }

    public class ProcessInBackground extends AsyncTask<Integer, Void, Exception> {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        Exception exception = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            progressDialog.setMessage("Busy loading rss feed...please wait...");
//            progressDialog.show();
        }

        @Override
        protected Exception doInBackground(Integer... integers) {

            try
            {
                URL url = new URL("https://web01.up.edu.ph/index.php/category/news/feed/");

                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

                factory.setNamespaceAware(false);

                XmlPullParser xpp = factory.newPullParser();

                xpp.setInput(getInputStream(url), "UTF_8");

                boolean insideItem = false;

                int eventType = xpp.getEventType();
                String data;

                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if (eventType == XmlPullParser.START_TAG) {
                        if (xpp.getName().equalsIgnoreCase("item")) {
                            insideItem = true;
                        }
                        else if (xpp.getName().equalsIgnoreCase("title")) {
                            if (insideItem) {
                                data = xpp.nextText();
//                                data = data.replaceAll("<(.*?)\\>"," ");//Removes all items in brackets
//                                data = data.replaceAll("<(.*?)\\\n"," ");//Must be undeneath
//                                data = data.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket
//                                data = data.replaceAll("&nbsp;"," ");
//                                data = data.replaceAll("&amp;"," ");
//                                titles.add(data);
                                articles.add(new Article(data, ""));
                            }
                        }
                        else if (xpp.getName().equalsIgnoreCase("content:encoded")) {
                            if (insideItem) {
                                data = xpp.nextText();
                                data = data.replaceAll("<(.*?)\\>"," ");//Removes all items in brackets
                                data = data.replaceAll("<(.*?)\\\n"," ");//Must be undeneath
                                data = data.replaceFirst("(.*?)\\>", " ");//Removes any connected item to the last bracket
                                data = data.replaceAll("&nbsp;"," ");
                                data = data.replaceAll("&amp;"," ");
//                                contents.add(data);
                                articles.get(articles.size()-1).setContent(data);
                            }
                        }
                    }
                    else if (eventType == XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase("item")) {
                        insideItem = false;
                    }

                    eventType = xpp.next();
                }


            }
            catch (MalformedURLException e) {
                exception = e;
            }
            catch (XmlPullParserException e) {
                exception = e;
            }
            catch (IOException e) {
                exception = e;
            }

            return exception;
        }

        @Override
        protected void onPostExecute(Exception s) {
            super.onPostExecute(s);

            ArrayAdapter<Article> adapter = new ArticleArrayAdapter(getActivity(), 0, articles);
            lv.setAdapter(adapter);

//            progressDialog.dismiss();
        }
    }
}
